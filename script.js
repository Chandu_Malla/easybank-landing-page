function toggleMenuAndImage() {
    var navigation = document.querySelector('#navigation');
    var logoImage = document.getElementById('logoImage');
    var crossImage = document.getElementById('crossImage');
    var mobileImage = document.getElementById('mobile-image');
  
    if (navigation.style.display === 'none' || navigation.style.display === '') {
      navigation.style.display = 'flex';
      logoImage.style.display = 'none';
      crossImage.style.display = 'block';
      mobileImage.style.display = 'none'
    } else {
      navigation.style.display = 'none';
      logoImage.style.display = 'block';
      crossImage.style.display = 'none';
      mobileImage.style.display = 'block'
    }
  }
  